const hamburger = document.querySelector("#hamburger");
const menu = document.querySelector("#menu");
const moon = document.querySelector("#moon");
const body = document.body;
const hLinks = document.querySelectorAll("#hLink");

hamburger.addEventListener("click", () => {
  menu.classList.toggle("hidden");
  hamburger.classList.toggle("bg-white");
});

hLinks.forEach((link) => {
  link.addEventListener("click", () => {
    menu.classList.toggle("hidden");
    hamburger.classList.toggle("bg-white");
  });
});

// Check if there's a cookie for the mode
const savedMode = localStorage.getItem("mode");

if (savedMode === "dark") {
  body.classList.add("dark");
} else {
  if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
    body.classList.add("dark");
    localStorage.setItem("mode", "dark");
  }
}

moon.addEventListener("click", () => {
  const currentMode = localStorage.getItem("mode");

  if (currentMode === "dark") {
    // Clear the cookie
    localStorage.removeItem("mode");
  } else {
    // Set the cookie to 'dark'
    localStorage.setItem("mode", "dark");
  }

  // Toggle the 'dark' class based on the current state
  if (localStorage.getItem("mode") === "dark") {
    body.classList.add("dark");
  } else {
    body.classList.remove("dark");
  }
});
